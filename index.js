const width = 40;
const height = 30;

const print = (value) => [
    [ 0, '&nbsp' ],
    [ 0.107, '-' ],
    [ 0.237, '~' ],
    [ 0.328, '+' ],
    [ 0.536, 'o' ],
    [ 0.691, '#' ],
    [ 0.832, '&' ],
    [ 1., '@' ],
  ].reduce(
    ([resVal, resChar], [val, char]) => (
        (getDistance) => getDistance(resVal) < getDistance(val)
      )((v) => Math.abs(value - v))
      ? [resVal, resChar]
      : [val, char]
    , [1, '']
  )[1];

const getShadedSphere = (x, z) => (
  (y) => 1 - Math.acos((-1 * x - 1 * y + 1 * z) / Math.sqrt(3) / Math.sqrt(x ** 2 + y ** 2 + z ** 2)) / Math.PI
)(((r) => - Math.sqrt(r ** 2 - x ** 2 - z ** 2))(12));

const points = [...Array(height)].map((_, posZ) => [...Array(width)].map((_, posX) => (
    (x, y) => print(getShadedSphere(x, y) || 0)
  )(posX + 0.5 - width / 2, -(posZ + 0.5) + height / 2)
));

document.write(points.map((row) => row.join('')).join('<br />'));
